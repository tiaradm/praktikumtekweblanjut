import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProductDetailComponent } from '../product-detail/product-detail.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  title:any;
  book:any={};
  //membuat koleksi books
  books:any=[];
  constructor(
    public dialog:MatDialog
  ) {
  }

  ngOnInit(): void {
    this.title= 'Product';
    this.book={
      title: 'Ibu Bumi',
      author: 'Tiara Dyah Murdaningrum',
      publisher: 'Gramedia',
      year: 2022,
      isbn: '00022110',
      price: 90000
    };
    this.getBooks();
  }

  getBooks()
  { 
    // memperbarui koleksi books
    this.books=[
      {
        title: 'Ibu Bumi',
      author: 'Tiara Dyah Murdaningrum',
      publisher: 'Gramedia',
      year: 2022,
      isbn: '00022110',
      price: 90000
      },
      {
      title: 'Jaga Diri Jaga Alam',
      author: 'Tiara Dyah Murdaningrum',
      publisher: 'Gramedia',
      year: 2023,
      isbn: '00028012',
      price: 100000
      }
    ];
  }

  productDetail(data:any, idx:any)
  {
    let dialog=this.dialog.open(ProductDetailComponent, {
      width:'400px',
      data:data
    });
    dialog.afterClosed().subscribe(res=> {
      if(res)
      {
        //jika idx= -1 (penambahan data baru) maka tambahkan data
        if(idx==-1)this.books.push(res);
        //jika tidak maka perbarui data
        else this.books[idx]=res;
      }
    })
  }

  deleteProduct(idx:any)
  {
    var conf=confirm('Delete item?');
    if(conf)
    this.books.splice(idx, 1)
  }

}
